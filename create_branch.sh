#!/bin/sh
issuKey="SHOW-125"

git config --global user.email "bamboo@kuhfahl.info"
git config --global user.name "demobuild"
git branch $issueKey
git checkout $issueKey
git commit --allow-empty -m "Create Branch for Issue $issueKey"
git push -u git@bitbucket.org:demonstration/demonstration.git $issueKey